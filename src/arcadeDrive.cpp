#include <Arduino.h>
#include "arcadeDrive.h"
#include "board.h"

double mDeadband    = 0.2;  // larger value = larger X/Y zero zone read from Pixy = less car jittering
bool mRightTurn     = true; // indicates last turn direction of car

bool mActiveSearch  = false;// indicates if car is in search mode
bool mStopSearching = false;// indicates the car should stop searching (set true after period of time)
uint32_t  mSearchPeriod  = 5000; // car will search for a target for this milliseconds
unsigned long mSearchStartTime; // indicates when car entered active search mode

static double limit(double value);
static double applyDeadband(double value, double deadband);
static double squareSpeedInputs(double value);
static void setMotorRotations(double left, double right);
static void stop(void);

/**
 * Arcade drive method.
 * xSpeed       : car's speed along the X axis [-1.0..1.0]. Forward is positive.
 * zRotation    : car's rotation rate around the Z axis [-1.0..1.0]. Clockwise
 *                is positive.
 * squareInputs : if true decreases the input sensitivty at low speeds
 */
void arcadeDrive(double xSpeed, double zRotation, bool squareInputs) {
  //limit speed to -1.0 to 1.0 and apply deadband around target center
  xSpeed = limit(xSpeed);
  xSpeed = applyDeadband(xSpeed, mDeadband);
  zRotation = limit(zRotation);
  zRotation = applyDeadband(zRotation, mDeadband);
  
  if (squareInputs) {
    xSpeed = squareSpeedInputs(xSpeed);
    zRotation = squareSpeedInputs(zRotation);
  }

  double leftMotorOutput, rightMotorOutput;
  double maxInput = max(abs(xSpeed), abs(zRotation));
  if (xSpeed < 0.0) {
    maxInput = -maxInput;
  }

  if (xSpeed >= 0.0) {
    if (zRotation >= 0.0) { //first quadrant
      mRightTurn = true;
      leftMotorOutput = maxInput;
      rightMotorOutput = xSpeed - zRotation;
    } else { //second quadrant
      mRightTurn = false;
      leftMotorOutput = xSpeed + zRotation;
      rightMotorOutput = maxInput;
    }
  } else {
    if (zRotation >= 0.0) { //third quadrant
      mRightTurn = true;
      leftMotorOutput = xSpeed + zRotation;
      rightMotorOutput = maxInput;
    } else { //forth quadrant
      mRightTurn = false;
      leftMotorOutput = maxInput;
      rightMotorOutput = xSpeed - zRotation;
    }
  }
  //set the motor rotation direction
  setMotorRotations(leftMotorOutput, rightMotorOutput);
  //now apply the calculated power to each side
  double leftSpeed = (abs(leftMotorOutput) * mMaxMotorSpeed);
  if (leftSpeed != 0 && leftSpeed < mMinMotorSpeed) {
    leftSpeed = mMinMotorSpeed;
  }
  double rightSpeed = (abs(rightMotorOutput) * mMaxMotorSpeed);
  if (rightSpeed != 0 && rightSpeed < mMinMotorSpeed) {
    rightSpeed = mMinMotorSpeed;
  }
  analogWrite(ENA, leftSpeed);
  analogWrite(ENB, rightSpeed);
}

//will start the car turning in place searching for a target
void searchForTarget() {
  //get time when method called
  unsigned long timeNow = millis();
  //if not already searching or stop search is set
  if (!mActiveSearch && !mStopSearching) {
    mActiveSearch = true;
    //set the start search time
    mSearchStartTime = timeNow;
    //being turning in the last turn direction of the car
    if (mRightTurn) {
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      //Right backward
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
    } else {
      //Left backward
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      //Right forward
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);  
    }
    analogWrite(ENB, (mMaxMotorSpeed * 2) / 4);
    analogWrite(ENA, (mMaxMotorSpeed * 2) / 4);
  } else { //search is active or stopped
    unsigned long searchDuration = timeNow - mSearchStartTime;
    //if search duration reached, stop car
    if (searchDuration > mSearchPeriod) {
      stop();
      mActiveSearch = false;
      mStopSearching = true;
    }
  }
}

//clear target search variables
void resetSearchVariables(void) {
  mActiveSearch = mStopSearching = false;
}

//stop car by turning off the motors
static void stop() {
  digitalWrite(ENA, LOW);
  digitalWrite(ENB, LOW);
}

//forces value to stay between -1.0 and 1.0
static double limit(double value) {
  if (value < -1.0) {
    return -1.0;
  } else if (value > 1.0) {
    return 1.0;
  }
  return value;
}

//returns 0.0 if the given value is within the specified deadband
//if between specified deadband and 1.0 then scale it 0 to 1.0
static double applyDeadband(double value, double deadband) {
  if (abs(value) > deadband) {
    if (value > 0.0) {
      return (value - deadband) / (1.0 - deadband);
    } else {
      return (value + deadband) / (1.0 - deadband);
    }
  }
  return 0.0; //within deadband
}

//square the inputs while preserving the sign, this will increase fine
//control while still allowing full power
static double squareSpeedInputs(double value) {
  double result = value * value;
  if (value < 0.0) {
    result = -result;
  }
  return result;
}

//sets the left and right side rotation based on sign of output power
static void setMotorRotations(double left, double right) {
  if (left < 0) {
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
  } else {
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
  }
  if (right < 0) {
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);
  } else {
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
  }
}