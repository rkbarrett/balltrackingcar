#include <Arduino.h>
#include <Pixy2.h>
#include "arcadeDrive.h"
#include "board.h"

Pixy2 pixy;                 //Pixy2 camera
Block mTargetBlock;         //target block from camera
int mSignature = 1;         //signature # set in Pixy2 to track

double mXmax        = 316;  // max X value of Pixy camera
double mXtarget     = 158;  // target X we want the ball (dead center)
double mYmax        = 208;  // max Y value of Pixy camera
double mYtarget     = 156;  // target Y we want the ball (towards bottom of camera view)
double mXinvert     = 1.0;  // set negative to invert X axis read from Pixy
double mYinvert     = -1.0; // set negative to invert Y axis read from Pixy

/*
 * Pixy2 block values
 * pixy.ccc.blocks[i].m_signature : signature of object detected (1 to 7)
 * pixy.ccc.blocks[i].m_x         : 0 to 316 left to right
 * pixy.ccc.blocks[i].m_y         : 0 to 208 top to bottom
 * pixy.ccc.blocks[i].m_width     : 1 to 316
 * pixy.ccc.blocks[i].m_height    : 1 to 208
 * pixy.ccc.blocks[i].m_angle     : -180 to 180 color code
 * pixy.ccc.blocks[i].m_index     : tracking index of the block
 * pixy.ccc.blocks[i].m_age       : number of frames object has been tracked
 * 
 * NOTE: do NOT use pin 11 with Pixy camera for motor control ENB, communication conflict
 */

// container for basic target information
struct TargetInfo {
  double xPosPixy, yPosPixy;  //X and Y positions reported by Pixy camera
  int width, height;          //width and height of object reported by Pixy camera
  int age;                    //number of frames Pixy has traced the object
  double xSpeed, ySpeed;      //calculated speed (-1.0 to 1.0) to intercept object 
};

static bool selectTarget(uint16_t signature);
static TargetInfo getTargetInfo(Block block);
static void printTargetVals(String str, TargetInfo ti);
static void printSpeeds(TargetInfo ti);

//setup Arduino
void setup() {
  Serial.begin(115200);
  Serial.print("Starting engine...\n");
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(ENA, OUTPUT);
  pinMode(ENB, OUTPUT);
  analogWrite(ENA, mMaxMotorSpeed);  
  analogWrite(ENB, mMaxMotorSpeed);    
  pixy.init();
}

//main loop
void loop() {
  // grab blocks from camera
  pixy.ccc.getBlocks();

  //if any blocks found
  if (pixy.ccc.numBlocks) {
    //if a target matching the signature is found
    if (selectTarget(mSignature)) {
      //reset searching variables
      resetSearchVariables();
      //extract information about the target from the Pixy block
      TargetInfo ti = getTargetInfo(mTargetBlock);
      //move car using Y as FWD/BKWD and X as rotation
      arcadeDrive(ti.ySpeed, ti.xSpeed, false);
      printTargetVals("Target", ti);
      printSpeeds(ti);
    } else {
      searchForTarget();
    }
  } else {
    searchForTarget();
  }
}


//choose target with matching signature, return true if target acquired
static bool selectTarget(uint16_t signature) {
  int age = 0;
  bool result = false;
  //evaluate each block detected by Pixy
  for (int i = 0; i < pixy.ccc.numBlocks; i++) {
    Block block = pixy.ccc.blocks[i];
    //if block matches signature specified by caller
    if (block.m_signature == signature) {
      //choose block that has been tracked the longest if multiple are found
      //TODO: use width/height ratio to narrow selected target (exclude robot bumbers)
      if (block.m_age > age) {
        mTargetBlock = block;
        age = block.m_age;
        result = true;
      }
    }
  }
  return result;
}

//get values from block and calculate X/Y intercept speeds
static TargetInfo getTargetInfo(Block block) {
  TargetInfo ti;
  ti.xPosPixy = (double)(block.m_x);
  ti.yPosPixy = (double)(block.m_y);
  ti.width = block.m_width;
  ti.height = block.m_height;
  ti.age = block.m_age; 

  // calculate X speed and normalize range -1.0 to 1.0, mXinvert used to flip range
  if (ti.xPosPixy > mXtarget) {
    ti.xSpeed = mXinvert * (ti.xPosPixy - mXtarget) / (mXmax - mXtarget);
  } else if (ti.xPosPixy < mXtarget) {
    ti.xSpeed = mXinvert * (ti.xPosPixy - mXtarget) / mXtarget;
  } else {
    ti.xSpeed = 0.0;
  }

  if (ti.yPosPixy > mYtarget) {
    ti.ySpeed = mYinvert * (ti.yPosPixy - mYtarget) / (mYmax - mYtarget);
  } else if (ti.yPosPixy < mYtarget) {
    ti.ySpeed = mYinvert * (ti.yPosPixy - mYtarget) / mYtarget;
  } else {
    ti.ySpeed = 0.0;
  }
  return ti;
}

//print helper to show values from Pixy
static void printTargetVals(String str, TargetInfo ti) {
  Serial.println(str + " X:" + String(ti.xPosPixy) + " Y:" + String(ti.yPosPixy) + " H:" + 
    String(ti.height) + " W:" + String(ti.width));
}

//print helper to show calculated X/Y speeds
static void printSpeeds(TargetInfo ti) {
  Serial.println("X: " + String(ti.xSpeed) + " Y: " + String(ti.ySpeed));
}