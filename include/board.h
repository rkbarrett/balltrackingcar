#ifndef BOARD_H
#define BOARD_H

//PIN assignments
#define ENA             5    //left motor PWM
#define ENB             10   //right motor PWM
#define IN1             6    //left motor rotation
#define IN2             7    //left motor rotation
#define IN3             8    //right motor rotation
#define IN4             9    //right motor rotation

#define mMinMotorSpeed  100  //hack to get car to move, below 100 motors hum don't turn
#define mMaxMotorSpeed  255  //max speed value to provide motors

#endif