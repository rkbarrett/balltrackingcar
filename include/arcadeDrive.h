#ifndef ARCADE_DRIVE_H
#define ARCADE_DRIVE_H

void arcadeDrive(double xSpeed, double zRotation, bool squareInputs);
void searchForTarget(void);
void resetSearchVariables(void);

#endif